﻿CREATE TABLE [dbo].[Teachers]
(
    [surname] TEXT NOT NULL, 
    [group] TEXT NULL, 
    [subject] TEXT NULL, 
    [number] TEXT NULL, 
    [grade] TEXT NULL, 
    [teacher] TEXT NULL, 
    CONSTRAINT [PK_Teachers] PRIMARY KEY ([surname])
)
