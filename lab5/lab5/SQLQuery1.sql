﻿INSERT INTO Grades (surname, group, subject, number, grade, teacher)
VALUES
('Ivanov', 'groupA', 'Math', 101, 90, 'Professor Smith'),
('Kurak', 'groupB', 'Math', 102, 75, 'Professor Smith'),
('Anudart', 'groupC', 'History', 103, 46, 'Professor Johnson'),
('Aliases', 'groupA', 'Physics', 104, 65, 'Professor White'),
('Kituki', 'groupB', 'History', 105, 95, 'Professor Johnson'),
('Outh', 'groupC', 'Physics', 106, 68, 'Professor White'),
('Liunth', 'groupA', 'Math', 107, 85, 'Professor Smith'),
('Petrov', 'groupB', 'History', 108, 75, 'Professor Johnson'),
('Paws', 'groupC', 'Psysics', 109, 62, 'Professor White'),
('Sidorov', 'groupA', 'Math', 110, 85, 'Professor Smith');